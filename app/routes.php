<?php

$app->get('/blogs/{id}/{title}','HomeController:blogs');
$app->get('/','HomeController:home');


$app->get('/hello/{name}', function ($request,$response,$args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");

    return $response;
});