<?php

namespace App\Controllers;

class HomeController extends Controller{

    public function blogs($req,$res,$args){
        
        $sth = $this->c->db->prepare("SELECT * FROM blog where id=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $blog = $sth->fetchAll();
        $relatedBlogs=$this->getBlogs(3);
        $res = $this->c->view->render($res, 'blog.phtml',['blog' => $blog,'related'=>$relatedBlogs]);
        return $res;
    }

    public function home($req,$res){
        
        $blogs=$this->getBlogs(6);
        $res = $this->c->view->render($res, 'home.phtml', ['blogs' => $blogs]);
        return $res;
    }

    // 
    private function getBlogs($lmt){
        $sth = $this->c->db->prepare("SELECT id,title,img,created_by,created_time FROM blog where is_deleted=0 ORDER BY created_time limit ".$lmt);
        
        $sth->execute();
        return  $sth->fetchAll();
    }
}